import Link from "next/link";
import { FC, ReactElement, ReactNode } from 'react';
// Components
import MainLayout from '../../components/layouts/MainLayout';

type Page = FC & { 
    getLayout?: (page: ReactElement) => ReactNode
}

const DeviceIndex: Page = () => {
    return (
        <>
            <h1>This is Device Page</h1>
            <Link href="/">
                <a>Go Home</a>
            </Link>
        </>
    )
}

DeviceIndex.getLayout = function getLayout(page: ReactElement) {
    return (
      <MainLayout>
        {page}
      </MainLayout>
    )
  }

export default DeviceIndex