import Link from "next/link";
import { useRouter } from 'next/router'
import { FC, ReactElement, ReactNode } from 'react';
// Components
import MainLayout from '../../components/layouts/MainLayout';

type Page = FC & { 
    getLayout?: (page: ReactElement) => ReactNode
}

const DeviceShow: Page = () => {
    const router = useRouter()
    const { id } = router.query

    return (
        <>
            <h1>Showing is Device {id}</h1>
            <Link href="/">
                <a>Go Home</a>
            </Link>
        </>
    )
}

DeviceShow.getLayout = function getLayout(page: ReactElement) {
    return (
      <MainLayout>
        {page}
      </MainLayout>
    )
}

export default DeviceShow;