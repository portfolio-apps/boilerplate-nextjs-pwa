import Link from "next/link";
import type { NextPage } from 'next'
import { FC, ReactElement, ReactNode } from 'react';
// Components
import MainLayout from '../components/layouts/MainLayout';

type Page = FC & { 
    getLayout?: (page: ReactElement) => ReactNode
}

const About: Page = () => {
  return (
    <>
        <h1>This is About Page</h1>
        <Link href="/">
            <a>Go Home</a>
        </Link>
    </>
  )
}

About.getLayout = function getLayout(page: ReactElement) {
    return (
      <MainLayout>
        {page}
      </MainLayout>
    )
  }

export default About