import React, { useContext, useMemo } from "react";

export const AppContext = React.createContext({} as any)

interface IAppProvider {
    children: React.ReactNode;
}

export function AppProvider({ children }: IAppProvider) {

    const title = React.useState('Ngauge');


    const memoedValue = useMemo(
        () => ({
            title
        }),
        [
            title
        ]
      );

    return (
        <AppContext.Provider value={memoedValue}>{children}</AppContext.Provider>
    );
}

export function useAppContext(): any {
    return useContext(AppContext);
}